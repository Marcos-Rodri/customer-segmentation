# Customer Segmentation

Customer Segmentation Project - Shopping Mall (with Python and Tableau)


In this notebook, I created a customer segmentation project for a shopping mall. This study's goal is to categorize the customers into clusters, in order to identify in which groups the mall should invest, so as to motivate them to spend more at its stores and facilities.


Firstly, we will visualize the client's characteristics (age, gender, annual income and spending score) based on the overall dataset. The spending score is a range from 0 (minimum consumption) to 100 (maximum consumption) that indicates which clients spent more money at the mall. Secondly, we will group the customers into clusters. At last, we will analyze each cluster individually to answer the following questions:


1) If we had to choose only two, in which clusters should we invest?


2) The marketing department needs more information about the selected clusters in order to trace action plans. Considering this, which are the particular characteristics of these clusters?


Data Source: This dataset can be found at: https://www.kaggle.com/vjchoudhary7/customer-segmentation-tutorial-in-python

